#include <iostream>
#include <chrono>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char* argv[]) {
	/*
	//chrono timer stuff!!
	chrono::time_point<chrono::system_clock> start, end; //time points store the current time of the system time clock
	int answer1 = 0, answer2 = 0;

	start = chrono::system_clock::now(); //now function returns the value of the system clock
	for (int i = 0; i < 500000; i++) {
		answer1 += i * (answer1 + 1);
	}
	end = chrono::system_clock::now();

	chrono::duration<double> elapsed = end - start; //duration is a measure between two time points

	cout << "Normal addition time elapsed: " << elapsed.count() << endl; //duration.count() returns the time elapsed
	cout << answer1 << endl;
*/

	vector<string> friendNames; //initializing a vector of strings
	friendNames.push_back("Robert"); //push_back adds an element to the 
	friendNames.push_back("Francis");
	friendNames.push_back("Jacob");
	friendNames.push_back("Zake");
	friendNames.push_back("Kelley");
	friendNames.push_back("Kevin");

	cout << friendNames[1] << endl; // use [] operators to index
	cout << friendNames.at(3) << endl; // at() is the same as indexing
	cout << friendNames.front() << endl; //front() returns the first value in the vector
	cout << friendNames.back() << endl; //back() returns the last value in the vector
	friendNames.insert(friendNames.begin()+2, "Rebecca"); //insert puts a value at an index , begin() is an iterator variable that stores the position of the first element
	friendNames.pop_back(); //pop_back() removes a value from the end of the vector
	friendNames.erase(friendNames.begin() + 3); //erase a value at an index
	cout << friendNames.size() << endl; //returns the size of the vector

	friendNames.clear();

	return 0;
}