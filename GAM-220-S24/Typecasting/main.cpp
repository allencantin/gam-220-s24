#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
	int num = 10;
	float myFloat = 6.7f;
	cout << num << endl;
	num = (int)myFloat; //C-style / explicit typecast
	cout << num << endl;
	num = int(myFloat); //function style typecast
	cout << num << endl;

	//static_cast
	num = static_cast<int>(myFloat); //good when you know exactly what you are casting

	//dynamic_cast
	//num = dynamic_cast<int>(myFloat); happens at runtime, good for classes/objects and pointers

	//const_cast gets rid of the const!
	//const int* immovablePointer = &num;
	//int* newPointer = const_cast<int*>(immovablePointer);

	//reinterpret cast
	// for pointers of incompatible types
	/*int* coolNumber = new int;
	*coolNumber = 88;

	string* name = new string;
	*name = "Allen";

	coolNumber = reinterpret_cast<int*>(name);
	cout << *coolNumber << endl;*/
	//num = 10;
	//myFloat = 6.7f;
	//myFloat = reinterpret_cast<float>(num);

	delete coolNumber;
	delete name;
}