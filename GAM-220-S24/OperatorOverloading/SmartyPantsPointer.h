#pragma once
template <typename T> //T is very generic and common
//we will use T every time we want to do something with the data that is stored here
class SmartyPantsPointer {
public:
	SmartyPantsPointer() {
		thePointer = new T;
	}
	~SmartyPantsPointer() {
		delete thePointer;
	}
	void operator++() {
		thePointer++;
	}
	void operator--() {
		thePointer--;
	}
	T GetValue() {
		return *thePointer;
	}
	void SetValue(T newValue) {
		*thePointer = newValue;
	}
private:
	T* thePointer;
};