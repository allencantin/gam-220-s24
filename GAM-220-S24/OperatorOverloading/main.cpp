#include <iostream>
#include "SmartyPantsPointer.h"

using namespace std;

class Position {
public:
	float x, y, z;
	Position() {
		x = 0;
		y = 0;
		z = 0;
	}
	Position(float _x, float _y, float _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	Position operator+(Position otherPosition) {
		Position answer;
		answer.x = x + otherPosition.x;
		answer.y = y + otherPosition.y;
		answer.z = z + otherPosition.z;
		return answer;
	}
	Position operator-(Position otherPosition) {
		Position answer;
		answer.x = x - otherPosition.x;
		answer.y = y - otherPosition.y;
		answer.z = z - otherPosition.z;
		return answer;
	}
};


int main(int argc, char* argv[]) {
	Position playerPosition(5, 2, 3);
	Position box(1, 0, 1);
	Position newPlace =  playerPosition + box;

	cout << newPlace.x << ", " << newPlace.y << ", " << newPlace.z << endl;

	//creating a smart pointer object of type integer
	SmartyPantsPointer<int> numberOfDonuts;
	numberOfDonuts.SetValue(12);
	cout << "Number of donuts: " << numberOfDonuts.GetValue() << endl;
}