#include <iostream>
#include <string>
#include <vector>

using namespace std;

//base class
class Food {
public:
	std::string name;
	int calories;
	enum Flavor {
		sweet,
		sour,
		bitter,
		savory,
		swag,
		meaty
	};
	Flavor flavor;

	Food() {
		name = "mush";
		calories = 100;
		flavor = sour;
	}

	Food(std::string newName, int cal, Flavor aFlavor) {
		name = newName;
		calories = cal;
		flavor = aFlavor;
	}
protected:
	int protein;
};

class Burger : public Food {
public:
	bool isVegetarian;
	int slicesOfCheese;
	bool lettuce;
	bool tomato;
	enum Doneness {
		rare,
		medium_rare,
		medium,
		medium_well,
		well_done
	};
	Doneness cooked;

	Burger() {
		//base variables
		name = "Burger";
		calories = 50000;
		flavor = meaty;
		protein = 50;

		//derived variables
		isVegetarian = false;
		slicesOfCheese = 1;
		lettuce = true;
		tomato = false;
		cooked = medium;
	}
};

class User {
public:
	string Name;
	int ID;
	vector<string> classList;

	User() {
		Name = "Bill";
		ID = 00;
	}
	User(string newName, int newID) {
		Name = newName;
		ID = newID;
	}

	//viewing stuff functionality
	//these are virtual so student and teacher classes can override them!
	virtual void ViewClassList() {
		cout << "This is a base user, can't do that" << endl;
	}
	virtual void ViewClassContent() {
		cout << "This is a base user, can't do that" << endl;
	}
};

class Student : public User {
public:
	Student(string newName, int newID) {
		Name = newName;
		ID = newID;
	}

	void ViewClassList() {
		for (int i = 0; i < classList.size(); i++) {
			cout << (i+1) << " : " << classList[i] << endl;
		}
	}

	void ViewClassContent() {
		cout << "Here is all the stuff you have to do!" << endl;
	}
};

class Teacher : public User {
public:
	Teacher(string newName, int newID) {
		Name = newName;
		ID = newID;
	}

	void ViewClassList() {
		for (int i = 0; i < classList.size(); i++) {
			cout << (i + 1) << " : " << classList[i] << endl;
		}
		cout << "TODO: add yourself to a class register" << endl;
	}
	
	void ViewClassContent() {
		cout << "This is where you assign stuff for student to do!" << endl;
	}

};

int main(int argc, char* argv[]) {

	/*Burger swagBurger;
	swagBurger.flavor = Burger::Flavor::swag;
	swagBurger.cooked = Burger::rare;

	if (swagBurger.cooked != Burger::well_done) {
		while (true) {
			std::cout << "burger";
		}
	}
	else
		return 0;*/

	Student kyle("Kyle", 01);
	kyle.classList.push_back("GAM220");
	kyle.classList.push_back("GAM420");

	Student alex("Alex", 02);
	alex.classList.push_back("GAM220");
	alex.classList.push_back("GDE210");

	Student jon("Jon", 03);
	jon.classList.push_back("GAM220");
	jon.classList.push_back("Mystery Class");

	Teacher allen("Allen", 2222);
	allen.classList.push_back("GAM110");
	allen.classList.push_back("GAM220");

	cout << "Kyle" << endl;
	kyle.ViewClassList();
	cout << "Alex" << endl;
	alex.ViewClassList();
	cout << "Jon" << endl;
	jon.ViewClassList();

	cout << "\nAllen" << endl;
	allen.ViewClassList();


	return 0;
}