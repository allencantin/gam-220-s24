#include <iostream>
#include <string>
#include <array>

using namespace std;

int main(int argc, char* argv[]) {
	int players = 5;
	int* playerPtr = &players;

	cout << *playerPtr;

	//delete playerPtr; DON'T DO THIS!! THIS IS ON STACK! >:(

	//we want a space of memory on the heap to store an address for an integer
	int* cookies = new int;

	cout << cookies << endl;
	*cookies = 10000;
	cout << *cookies << endl;

	delete cookies; //deallocating memory here
	cookies = nullptr; //DO THIS PLEASE  -setting a safe null value

	//cookies = NULL;  //DON'T DO THIS

	//int* scoreboard = new int(4); //cool pointer array
	//scoreboard[0] = 10;
	//scoreboard[1] = 20;
	//scoreboard[2] = 30;
	//scoreboard[3] = 40;
	//int size = sizeof(scoreboard) / sizeof(int);
	//for (int i = 0; i < 4; i++) { //classic C# way to iterate through an array

	//}
	//cool C++ way!
	/*cout << *scoreboard << endl;
	scoreboard++;
	cout << *scoreboard << endl;
	scoreboard++;
	cout << *scoreboard << endl;
	scoreboard++;
	cout << *scoreboard << endl;
	scoreboard++;
	cout << *scoreboard << endl;
	scoreboard++;
	cout << *scoreboard << endl;*/

	/*char myFunnyString[16];
	for (int i = 0; i < 16; i++)
	{
		myFunnyString[i] = 'h';
	}
	myFunnyString[15] = ' ';
	cout << myFunnyString << endl;*/



	return 0;
}