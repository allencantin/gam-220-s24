#include <iostream>
#include "SDL.h"
#include "SDL_ttf.h"


int main(int argc, char* argv[]) {

	//dimensions of the window
	const int width = 800;
	const int height = 600;

	SDL_Window* window;
	//SDL_Surface* windowSurface;
	SDL_Renderer* windowRenderer;
	SDL_Texture* toadTextures[3];
	int toadIndex = 0; //keep track of what toad we're on

	int mouseX = -1, mouseY = -1;

	//initializing SDL to start
	SDL_Init(SDL_INIT_VIDEO);
	//initialize TTF extension!!!
	TTF_Init();

	//create the window!
	window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
	//SDL_GetError() very helpful for debugging!!!
	//attach the surface to the window
	//windowSurface = SDL_GetWindowSurface(window);

	//cool new renderer
	//need to pass in the window it's attached to, -1 for renderer processing, and sdl flags
	windowRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(windowRenderer, 0xFF, 0xFF, 0xFF, 0xFF);


	TTF_Font* theFont;
	theFont = TTF_OpenFont("MouldyCheeseRegular-WyMWG.ttf", 28);

	SDL_Surface* tempTextSurface = TTF_RenderText_Solid(theFont, "I'M TOAD", { 255, 0, 0, 255 });
	SDL_Texture* toadText = SDL_CreateTextureFromSurface(windowRenderer, tempTextSurface);
	SDL_Rect textArea = { 10, 10, 280, 100 };
	//put color on the window
	//SDL_FillRect(windowSurface, NULL, SDL_MapRGB(windowSurface->format, 70, 10, 55));

	//fill the window with not-toad
	SDL_Surface* temporarySurface = SDL_LoadBMP("not-toad.bmp");
	toadTextures[0] = SDL_CreateTextureFromSurface(windowRenderer, temporarySurface);
	temporarySurface = SDL_LoadBMP("not-toad-green1.bmp");
	toadTextures[1] = SDL_CreateTextureFromSurface(windowRenderer, temporarySurface);
	temporarySurface = SDL_LoadBMP("not-toad-green2.bmp");
	toadTextures[2] = SDL_CreateTextureFromSurface(windowRenderer, temporarySurface);

	//draw the image for real
	//SDL_BlitSurface(toadSurfaces[0], NULL, windowSurface, NULL);

	SDL_RenderCopy(windowRenderer, toadTextures[0], NULL, NULL);
	SDL_RenderCopy(windowRenderer, toadText, NULL, &textArea);
	SDL_RenderPresent(windowRenderer);
	//applies the changes to the window
	SDL_UpdateWindowSurface(window);

	//hold onto any event that happens
	SDL_Event e;

	bool quit = false;
	while (quit == false) 
	{ 
		//events loop!!
		while (SDL_PollEvent(&e)) //process all events on the computer and store the event in e
		{ 
			if (e.type == SDL_QUIT) { //if user hits x on window
				quit = true;
				break;
			}

			//let's check for keyboard input
			if (e.type == SDL_KEYDOWN) {
				//was it G?
				if (e.key.keysym.sym == SDLK_g) {
					//they pressed G
					if (toadIndex + 1 < 3) {
						toadIndex++;
						//SDL_BlitSurface(toadTextures[toadIndex], NULL, windowSurface, NULL);
						SDL_RenderCopy(windowRenderer, toadTextures[toadIndex], NULL, NULL);
						SDL_RenderCopy(windowRenderer, toadText, NULL, &textArea);
						SDL_RenderPresent(windowRenderer);
					}
				}
				if (e.key.keysym.sym == SDLK_b) {
					//they pressed B
					if (toadIndex - 1 > -1) {
						toadIndex--;
						SDL_RenderCopy(windowRenderer, toadTextures[toadIndex], NULL, NULL);
						SDL_RenderCopy(windowRenderer, toadText, NULL, &textArea);
						SDL_RenderPresent(windowRenderer);
					}
				}
			}


			//let's check for mouse click
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				//where is the mouse that was clicked?
				SDL_GetMouseState(&mouseX, &mouseY);
				std::cout << mouseX << ", " << mouseY << std::endl;
			}
		}
	}

	//freeing memory
	SDL_DestroyWindow(window);

	SDL_DestroyTexture(toadTextures[0]);
	SDL_DestroyTexture(toadTextures[1]);
	SDL_DestroyTexture(toadTextures[2]);
	SDL_DestroyTexture(toadText);
	SDL_FreeSurface(temporarySurface);
	SDL_FreeSurface(tempTextSurface);

	//free the font pointer
	TTF_CloseFont(theFont);

	TTF_Quit();
	SDL_Quit(); //cleans up everything else

	return 0;
}