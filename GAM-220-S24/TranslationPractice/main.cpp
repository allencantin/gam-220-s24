#include <iostream> //translation of using system
#include <string>   //need this for more string functionality!

using namespace std; //allows us to use cout and cin and many other things without std:: prefix

int main(int argc, char* argv[]) { //cool new main function!

	cout << "Hello World!" << endl;//use cout to print to console

	//variables work the same (arrays are different with brackets at the end)

	int number = 10;

	float anotherNum = 9.81f;

	string words = "blah blah blah";

	cout << words << endl;

	string capital = "";
	cout << "What is the capital of Germany?" << endl;

	cin >> capital; //use cin to get input from console

	cout << "Input recieved: " << capital;

	number = stoi(capital); //stoi is our new int.Parse()!

	return 0; //make sure to return the good code 0 at the end
}
