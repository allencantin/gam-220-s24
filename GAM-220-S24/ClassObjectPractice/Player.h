#pragma once
#include <string>

class Player {
public:
	Player(); //constructor
	Player(std::string newName, int startHealth, int newDamage);
	~Player(); //destructor
	int Attack(Player &opponent);
	void TakeDamage(int dmg);
	int GetHealth();
private:
	std::string playerName;
	int health;
	int damage;
};