#include <iostream>
#include "Game.h"

using namespace std;

int main(int argc, char* argv[]) {
	cout << "we are gaming!!" << endl;
	Game myGame; //we don't have constructor arguments
	Player ALLEN("Allen", 100, 10);
	Player JOHN;
	myGame.AddPlayer(ALLEN);
	myGame.AddPlayer(JOHN);
	myGame.Play();


	return 0;
}