#include "Game.h"

//scope resolution
Game::Game() { //constructor
	player1 = nullptr;
	player2 = nullptr;
}

Game::~Game() { //deconstructor
	//delete player1;
	//delete player2;
}

void Game::Play()
{
	if (player1 == nullptr || player2 == nullptr)
		return;
	int currentPlayer = 1;
	while (!IsGameOver()) {
		std::cout << "It is Player " << currentPlayer << "'s turn to attack!" << std::endl;
		if (currentPlayer == 1) {
			std::cout<<"Player 1 did " << player1->Attack(*player2) << " damage." << std::endl;
		}
		else {
			std::cout << "Player 2 did " << player2->Attack(*player1) << " damage." << std::endl;
		}
		std::cin;
		if (currentPlayer == 1)
			currentPlayer = 2;
		else
			currentPlayer = 1;
	}
}

void Game::AddPlayer(Player& newPlayer)
{
	if (player1 == nullptr)
		player1 = &newPlayer;
	else if (player2 == nullptr)
		player2 = &newPlayer;
}

bool Game::IsGameOver()
{
	if (player1->GetHealth() > 0 && player2->GetHealth() > 0)
		return false;

	return true;
}
