#include "Player.h"

Player::Player() {
	playerName = "John";
	health = 100;
	damage = 5;
}

Player::Player(std::string newName, int startHealth, int newDamage) {
	playerName = newName;
	health = startHealth;
	damage = newDamage;
}

Player::~Player() {

}

int Player::Attack(Player &opponent)
{
	//we attack and affect opponent's health
	opponent.TakeDamage(damage);
	//return the damage dealt
	return damage;
}

void Player::TakeDamage(int dmg)
{
	health -= dmg;
}

int Player::GetHealth()
{
	return health;
}
