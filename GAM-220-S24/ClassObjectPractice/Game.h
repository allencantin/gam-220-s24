#pragma once
#include <string>
#include "Player.h"
#include <iostream>

class Game {
public:
	Game();
	~Game();
	void Play(); //function declaration for a void return type
	void AddPlayer(Player &newPlayer); //function declaration for a function with parameters

private:
	bool IsGameOver(); //declaration with return type
	int playerCount = 0;
	Player* player1;
	Player* player2;
};