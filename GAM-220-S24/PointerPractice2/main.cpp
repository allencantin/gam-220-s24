#include <iostream>

using namespace std;

void MoveThePointer(char& thePointer) { //little example of a function
	thePointer++;
}

int main(int argc, char* argv[]) {

	/*char* funnyPointer = new char;
	int counter = 0;
	while (counter < 50) {
		cout << (int)*funnyPointer << "  " << *funnyPointer << endl;
		MoveThePointer(*funnyPointer);
		cout << (int)*funnyPointer << "  " << *funnyPointer << endl;
		counter++;
	}
	delete funnyPointer;*/

	char funnyWord[7] = { 'R', 'o', 'b', 'e', 'r', 't', '!' };
	char* funnyPtr = &funnyWord[6];
	cout << funnyWord << endl;
	
	int counter = 0;
	while (counter < 1) {
		if (*funnyPtr == '\0') {
			*funnyPtr = '\n';
			counter++;
		}
		funnyPtr++;
	}

	cout << funnyWord << endl;

	return 0;
}
