#include <iostream>
#include <string>

using namespace std;

int add(int num1, int num2) {
	//recursion
	//if (num1 & num2 > 0) GOOD THINKING
	//carry-overs
	int carryovers = (num1 & num2) << 1;
	int sum = num1 ^ num2;

	if (carryovers == 0)
		return sum;
	else
		add(carryovers, sum);
	//if carryover variable is zero, this means we can XOR the numbers together and return
	//if not, call add again with carryovers and sum as arguments

	return sum;
}

int subtract(int num1, int num2) {
	//use the add function here
	if (num2 == 0)
		return num1;

	//int answer = num1 & (~num2);


	return answer;
}

int main(int argc, char* argv[]) {

	cout << "Calculator!!!" << endl;
	string expression = "";
	string numberCatcher = "";
	bool isAddition = false;
	cin >> expression;
	int operand1, operand2;
	//loop!
	for (int i = 0; i < expression.size(); i++) {
		//check for + or -
		if (expression[i] != '+' && expression[i] != '-') {
			//stow away number
			numberCatcher += expression[i];
		}
		else {
			//we have found an operator
			//atoi(numberCatcher.c_str()) - just in case
			operand1 = stoi(numberCatcher);
			numberCatcher = "";
			//check which operator it is
			if (expression[i] == '+')
				isAddition = true;
		}
		//store numbers into operands
	}
	operand2 = stoi(numberCatcher);

	//bitwise stuff
	if (isAddition) {
		//call the addition function
		cout << add(operand1, operand2) << endl;
	}
	else {
		//call the subtraction function
		cout << subtract(operand1, operand2) << endl;
	}
		//print out the result

	return 0;
}