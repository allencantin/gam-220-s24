#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
	//what about a phonebook?
	map<string, string> phonebook;
	phonebook.insert({ "Kyle", "555-555-5555" }); //insert values
	phonebook.insert({ "Jon", "444-444-4444" }); //insert values
	phonebook.insert({ "Alexander", "333-333-3333" }); //insert values

	//print out keys and values with a loop
	for (auto iterator = phonebook.begin(); iterator != phonebook.end(); ++iterator) {
		cout << iterator->first << " : " << iterator->second << endl;
	}


	return 0;
}